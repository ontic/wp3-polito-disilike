package it.polito.dbdmg.ontic.util

import org.apache.log4j.Level

class StatisticsCommandLineArguments (
  var masterUrl: String = null,
  var inputPath: String = null,
  var inputNormPath: Option[String] = None,
  var headerPath: Option[String] = None,
  var sparkLogLevel:Level = Level.WARN,
  var onticLogLevel:Level = Level.INFO,
  var parserClassName:String = "it.polito.dbdmg.ontic.io.ClusteringResultParser",
  var parserSeparator:String = ";",
  var withNoisePoints:Boolean = false,
  var noiseInputPath: String = null) {

}

class StatisticsCommandLineParser (programName:String, var args:StatisticsCommandLineArguments=new StatisticsCommandLineArguments) extends scopt.OptionParser[Unit](programName) {
  
  opt[String] ("ds-master")
    .foreach { args.masterUrl = _ }
    .required ()
    .valueName ("<url>")
    .text ("Master URL")

  opt[String] ("ds-input")
    .foreach { args.inputPath = _ }
    .required()
    .valueName("<path>")
    .text("Input path")

  opt[String] ("ds-inputNorm")
    .foreach { x => args.inputNormPath = Some(x) }
    .valueName("<path>")
    .text("Input path for a second file (normalized)")

  opt[String] ("headerPath")
    .foreach { x => args.headerPath = Some(x) }
    .valueName("<path>")
    .text("Input path for a file whose first line is a header for the features")

    
  opt[String] ("onticLoggerLevel").foreach {
    x => args.onticLogLevel = Level.toLevel(x)
  }.valueName("<level>")
  .text("The level used by the ontic logger.")
  
  opt[String] ("sparkLoggerLevel").foreach {
    x => args.sparkLogLevel = Level.toLevel(x)
  }.valueName("<level>")
  .text("The level used by the spark logger.")
  
  opt[String] ("parserClass").foreach {
    x => args.parserClassName = x
  }.valueName("<class>")
  .text("The class used for parsing the input.")

  opt[String] ("parserSeparator").foreach {
    x => args.parserSeparator = x
  }.valueName("<string>")
  .text("The separator assumed by the parser class.")
  
  opt[String] ("withNoise").foreach {
    x => args.withNoisePoints = x.toBoolean
  }.valueName("<bool>")
  .text("Whether to include the noise in the Silhouette computation or not.")
  
  opt[String] ("noiseInputPath").foreach {
    x => args.noiseInputPath = x
  }.valueName("<path>")
  .text("The location where noise points are located. All points with cluster id 0 in this location are considered noise. If there are points in the cluster 0 in other location, they will be ignored.")
  
}
