package it.polito.dbdmg.ontic.util

import org.apache.log4j.Level

class SilhouetteCommandLineArguments (
  var masterUrl: String = null,
  var inputPath: String = null,
  var outputPath: String = null,
  var sparkLogLevel:Level = Level.WARN,
  var onticLogLevel:Level = Level.INFO,
  var parserClassName:String = "it.polito.dbdmg.ontic.io.ClusteringResultParser",
  var parserSeparator:String = ";",
  var borderPath:String = null,
  var modelPath:String = null,
  var fraction:Double = 1.0,
  var numCandidates:Int = 100,
  var withNoisePoints:Boolean = false,
  var noiseInputPath: String = null) {

}

class SilhouetteCommandLineParser (programName:String, var args:SilhouetteCommandLineArguments=new SilhouetteCommandLineArguments) extends scopt.OptionParser[Unit](programName) {
  
  opt[String] ("ds-master")
    .foreach { args.masterUrl = _ }
    .required ()
    .valueName ("<url>")
    .text ("Master URL")

  opt[String] ("ds-input")
    .foreach { args.inputPath = _ }
    .required()
    .valueName("<path>")
    .text("Input path")

  opt[String] ("ds-output")
    .foreach { args.outputPath = _ }
    //.required() //TODO: add this field to all silhouette, then make this required
    .valueName("<path>").text("Output path")

  opt[String] ("ds-borderInput")
    .foreach { args.borderPath = _ }
    .valueName("<path>")
    .text("The path of the border points for border Silhouette")

  opt[String] ("ds-fraction")
    .foreach { x => args.fraction = x.toDouble }
    .valueName("<double>")
    .text("The fraction of border points to use for border Silhouette. Default: 1.0")

  opt[String] ("ds-numCandidates")
    .foreach { x => args.numCandidates = x.toInt }
    .valueName("<double>")
    .text("The number of points to be elected as border for Kmeans border Silhouette. Default: 100")

  opt[String] ("ds-modelInput")
    .foreach { args.modelPath = _ }
    .valueName("<path>")
    .text("The path of Kmeans model (list of centroids) for border Silhouette")
    
  opt[String] ("onticLoggerLevel").foreach {
    x => args.onticLogLevel = Level.toLevel(x)
  }.valueName("<level>")
  .text("The level used by the ontic logger.")
  
  opt[String] ("sparkLoggerLevel").foreach {
    x => args.sparkLogLevel = Level.toLevel(x)
  }.valueName("<level>")
  .text("The level used by the spark logger.")
  
  opt[String] ("parserClass").foreach {
    x => args.parserClassName = x
  }.valueName("<class>")
  .text("The class used for parsing the input.")

  opt[String] ("parserSeparator").foreach {
    x => args.parserSeparator = x
  }.valueName("<string>")
  .text("The separator assumed by the parser class.")
  
  opt[String] ("withNoise").foreach {
    x => args.withNoisePoints = x.toBoolean
  }.valueName("<bool>")
  .text("Whether to include the noise in the Silhouette computation or not.")
  
  opt[String] ("noiseInputPath").foreach {
    x => args.noiseInputPath = x
  }.valueName("<path>")
  .text("The location where noise points are located. All points with cluster id 0 in this location are considered noise. If there are points in the cluster 0 in other location, they will be ignored.")
  
}
