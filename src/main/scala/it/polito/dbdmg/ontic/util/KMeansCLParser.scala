package it.polito.dbdmg.ontic.util

import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings.NormalizationTechnique
import org.apache.log4j.Level


class KMeansArguments(
  var masterUrl: String = null,
  var inputPath: String = null,
  var outputPath: String = null,
  var normalizationTechnique:NormalizationTechnique.Value = NormalizationTechnique.None,
  var K:Int = 5,
  var parserClassName:String = "it.polito.dbdmg.ontic.io.TextToPointConverter",
  var parserSeparator:String = ",",
  var sparkLogLevel:Level = Level.WARN,
  var onticLogLevel:Level = Level.INFO
) {
  
  
}

class KMeansCLParser(programName:String, var args:KMeansArguments=new KMeansArguments) extends scopt.OptionParser[Unit](programName)  {
  opt[String] ("ds-master")
    .foreach { args.masterUrl = _ }
    .required ()
    .valueName ("<url>")
    .text ("Master URL")

  opt[String] ("ds-input")
    .foreach { args.inputPath = _ }
    .required()
    .valueName("<path>")
    .text("Input path")

  opt[String] ("ds-output")
    .foreach { args.outputPath = _ }
    .required()
    .valueName("<path>").text("Output path")
    
    opt[String] ("normalization").foreach {
    x => args.normalizationTechnique = NormalizationTechnique.withName(x)
  }.valueName("<enum>")
  .text("The normalization technique used by the preprocessor, if any.")
  
  opt[String] ("K").foreach {
    x => args.K = x.toInt
  }.valueName("<int>")
  .text("The number of centroids and cluster to be used.")
 
  opt[String] ("parserClass").foreach {
    x => args.parserClassName = x
  }.valueName("<class>")
  .text("The class used for parsing the input.")

  opt[String] ("parserSeparator").foreach {
    x => args.parserSeparator = x
  }.valueName("<string>")
  .text("The separator assumed by the parser class.")
  
  opt[String] ("sparkLoggerLevel").foreach {
    x => args.sparkLogLevel = Level.toLevel(x)
  }.valueName("<level>")
  .text("The level used by the spark logger.")
  
  opt[String] ("onticLoggerLevel").foreach {
    x => args.onticLogLevel = Level.toLevel(x)
  }.valueName("<level>")
  .text("The level used by the ontic logger.")
  
}