package it.polito.dbdmg.ontic.preprocessing


import scala.NoSuchElementException
import org.apache.log4j.Logger
import org.apache.commons.math3.ml.distance.EuclideanDistance
import org.apache.commons.math3.ml.distance.DistanceMeasure
import org.alitouka.spark.dbscan.spatial.DistanceMeasureSuite
import org.alitouka.spark.dbscan.spatial.EuclideanDistanceSuite


object DBSCANPreprocessingSettings {
  
  object EpsilonPolicies extends Enumeration {
    type EpsilonPolicies = Value
    val MaxStepValue, Threshold, MaxStepOverThreshold, 
          BeforeMaxHole, BeforeMaxDensityDecrease, MaxDensity,
            SquaredMean, StdDevOverMean, BeforeFirstDensityDecrease, BeforeFirstDensityDecrease25 = Value
  }
  object MinPointsPolicies extends Enumeration {
    type MinPointsPolicies = Value
    val MyGreedyFunc, MinDecrease, BeforeGreatestPercentageIncrement = Value
  }
  object NormalizationTechnique extends Enumeration {
    type NormalizationTechnique = Value
    val None, Zscore, MinMax = Value
  }
  
}

class DBSCANPreprocessingSettings extends Serializable {
  
  import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings._
  var epsStep:Double = 1.0
  var distanceMeasureSuite:DistanceMeasureSuite = new EuclideanDistanceSuite
  var epsilonPolicy = EpsilonPolicies.BeforeMaxDensityDecrease
  var minPtsPolicy = MinPointsPolicies.MyGreedyFunc
  var maxSuggestedEpsilon:Int = 3
  var numberOfPointsPerPartition:Long = 50000
  var normalizationTechnique = NormalizationTechnique.None
  
  
  def withNumberOfPointsPerPartition(n:Long) = {
    this.numberOfPointsPerPartition = n
    this
  }
  
  def withEpsStep(step:Double) = {
    this.epsStep = step
    this
  }
  def withMaxSuggestedEpsilon(maxSuggestedEpsilon:Int) = {
    this.maxSuggestedEpsilon = maxSuggestedEpsilon
    this
  }
   
  def withDistanceMeasureSuite(distanceMeasure:String) = {
    this.distanceMeasureSuite = Class.forName(distanceMeasure).getConstructor().newInstance().asInstanceOf[DistanceMeasureSuite]
    this
  }
  
  def withEpsilonPolicy(policy:String) = {
    try{
      epsilonPolicy=EpsilonPolicies.withName(policy)
    }catch{
      case e:NoSuchElementException => Logger.getLogger("it.polito.dbdmg.ontic.Logger").warn("Undefined epsilon policy "+policy+". Default value will be used.")
    }
    this
  }
  def withMinPointsPolicy(policy:String) = {
    try{
      minPtsPolicy=MinPointsPolicies.withName(policy)
    }catch{
      case e:NoSuchElementException => Logger.getLogger("it.polito.dbdmg.ontic.Logger").warn("Undefined minPoints policy "+policy+". Default value will be used.")
    }
    this
  }
  
  def withNormalizationTechnique(policy:String) = {
    try{
      normalizationTechnique=NormalizationTechnique.withName(policy)
    }catch{
      case e:NoSuchElementException => Logger.getLogger("it.polito.dbdmg.ontic.Logger").warn("Undefined NormalizationTechnique policy "+policy+". Default value will be used.")
    }
    this
  }
  
}