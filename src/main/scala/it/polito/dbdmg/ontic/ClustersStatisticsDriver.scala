package it.polito.dbdmg.ontic

import it.polito.dbdmg.ontic.io.ClusteredIOParser
import it.polito.dbdmg.ontic.postprocessing.SquaredSilhouette
import it.polito.dbdmg.ontic.statistics.ZDistribution
import it.polito.dbdmg.ontic.util.StatisticsCommandLineParser
import org.apache.log4j.Logger
import org.apache.spark.{SparkConf, SparkContext}
import SparkContext._
import org.apache.spark.mllib.stat.{MultivariateOnlineSummarizer, MultivariateStatisticalSummary, Statistics}
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import breeze.linalg._


object ClustersStatisticsDriver {
  def main(args:Array[String]):Unit = {
    val clArgs:StatisticsCommandLineParser = new StatisticsCommandLineParser("Ontic SquaredSilhouette")
    if(clArgs.parse(args)){
      val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
      
      logger.setLevel(clArgs.args.onticLogLevel)
      Logger.getRootLogger.setLevel(clArgs.args.sparkLogLevel)
      
      //creating spark context
      val conf=new SparkConf().setMaster(clArgs.args.masterUrl)
                    .setAppName("Ontic Spark Statistics")
      
      logger.debug("Spark master: " +clArgs.args.masterUrl)
      
      implicit val sc=new SparkContext(conf)
      val startingTime = System.currentTimeMillis()
      
      //reading data
      val parser = Class.forName(clArgs.args.parserClassName).getConstructor().newInstance().asInstanceOf[ClusteredIOParser]
      parser.separator = clArgs.args.parserSeparator
      val startingPoints = sc.textFile(clArgs.args.inputPath)
        .map { parser.parse }
//        .filter(t=>t._1!=0)
        .mapValues(v => Vectors.dense(v))
      
      val points = if(clArgs.args.withNoisePoints){
        startingPoints.union(sc.textFile(clArgs.args.noiseInputPath)
          .map { parser.parse }
          .filter(t=>t._1==0)
          .mapValues(v => Vectors.dense(v)))
      }else{
        startingPoints
      }.cache()

      val pointsNorm = if(clArgs.args.inputNormPath.isDefined){
        sc.textFile(clArgs.args.inputNormPath.get)
          .map { parser.parse }
//          .filter(t=>t._1!=0)
          .mapValues(v => Vectors.dense(v))
      }else{
        startingPoints
      }.cache()



      // Compute column summary statistics.
      val summary: MultivariateStatisticalSummary = Statistics.colStats(points.map(x => x._2))
      //      println(summary.mean) // a dense vector containing the mean value for each column
      //      println(summary.variance) // column-wise variance
      //      println(summary.numNonzeros) // number of nonzeros in each column
      val summaryN: MultivariateStatisticalSummary = Statistics.colStats(pointsNorm.map(x => x._2))

      val summaryByKey = points.aggregateByKey(new MultivariateOnlineSummarizer)(
      {case (s, v) => s.add(v)},
      {case (s1, s2) => s1.merge(s2)})
      val summaryByKeyN = pointsNorm.aggregateByKey(new MultivariateOnlineSummarizer)(
      {case (s, v) => s.add(v)},
      {case (s1, s2) => s1.merge(s2)})

//      summaryByKey.foreach{case (cl, sum) => println(f"Cluster $cl:\n${sum.mean}\n${sum.variance}")}

      val varBegin = new DenseVector(summaryN.variance.toArray)
      val varNow = summaryByKeyN.collectAsMap().mapValues(x => new DenseVector(x.variance.toArray))

      val varianceReduction = varNow.mapValues(x => x - varBegin)

      val varSortedByVarRed = varianceReduction.mapValues(_.toArray.zipWithIndex.sortBy(-_._1))
      val varSortedByVar = varNow.mapValues(_.toArray.zipWithIndex.sortBy(_._1))


      val columnNames = { if(clArgs.args.headerPath.isDefined){
        val header: Array[String] = sc.textFile(clArgs.args.headerPath.get).first().split(clArgs.args.parserSeparator.toCharArray)
        x:Int => header.apply(x)
      } else {
        x:Int => x.toString
      }}

      println("Cluster name; num.records; 1st attr. by Var Red index; average; 2nd attr. by Var Red index; average;3rd attr. by Var Red index; average; 1st by Var index; average; 2nd by Var index; average;3rd by Var index; average")
      summaryByKey.collectAsMap().foreach { case x =>
        val v1byvar = varSortedByVar(x._1)(0)._2
        val v2byvar = varSortedByVar(x._1)(1)._2
        val v3byvar = varSortedByVar(x._1)(2)._2
        val v1byvarRed = varSortedByVarRed(x._1)(0)._2
        val v2byvarRed = varSortedByVarRed(x._1)(1)._2
        val v3byvarRed = varSortedByVarRed(x._1)(2)._2
        val v1byvarAvg = x._2.mean(v1byvar)
        val v2byvarAvg = x._2.mean(v2byvar)
        val v3byvarAvg = x._2.mean(v3byvar)
        val v1byvarRedAvg = x._2.mean(v1byvarRed)
        val v2byvarRedAvg = x._2.mean(v2byvarRed)
        val v3byvarRedAvg = x._2.mean(v3byvarRed)
        println(f"cluster ${x._1}; ${x._2.count}; ${columnNames(v1byvarRed)}; $v1byvarRedAvg; ${columnNames(v2byvarRed)}; $v2byvarRedAvg; ${columnNames(v3byvarRed)}; $v3byvarRedAvg; ${columnNames(v1byvar)}; $v1byvarAvg; ${columnNames(v2byvar)}; $v2byvarAvg; ${columnNames(v3byvar)}; $v3byvarAvg")
      }

      println("Cluster name; num.records; 1st attr. by Var Red index; average; std.dev; 2nd attr. by Var Red index; average;std.dev;3rd attr. by Var Red index; average;std.dev; 1st by Var index; average;std.dev; 2nd by Var index; average;std.dev;3rd by Var index; average;std.dev")

      summaryByKey.collectAsMap().foreach { case x =>
        val v1byvar = varSortedByVar(x._1)(0)._2
        val v2byvar = varSortedByVar(x._1)(1)._2
        val v3byvar = varSortedByVar(x._1)(2)._2
        val v1byvarRed = varSortedByVarRed(x._1)(0)._2
        val v2byvarRed = varSortedByVarRed(x._1)(1)._2
        val v3byvarRed = varSortedByVarRed(x._1)(2)._2
        val v1byvarAvg = x._2.mean(v1byvar)
        val v2byvarAvg = x._2.mean(v2byvar)
        val v3byvarAvg = x._2.mean(v3byvar)
        val v1byvarVar = math.sqrt(x._2.variance(v1byvar))
        val v2byvarVar = math.sqrt(x._2.variance(v2byvar))
        val v3byvarVar = math.sqrt(x._2.variance(v3byvar))
        val v1byvarRedAvg = x._2.mean(v1byvarRed)
        val v2byvarRedAvg = x._2.mean(v2byvarRed)
        val v3byvarRedAvg = x._2.mean(v3byvarRed)
        val v1byvarRedVar = math.sqrt(x._2.variance(v1byvarRed))
        val v2byvarRedVar = math.sqrt(x._2.variance(v2byvarRed))
        val v3byvarRedVar = math.sqrt(x._2.variance(v3byvarRed))
        println(f"cluster ${x._1}; ${x._2.count}; ${columnNames(v1byvarRed)}; $v1byvarRedAvg; $v1byvarRedVar; ${columnNames(v2byvarRed)}; $v2byvarRedAvg; $v2byvarRedVar; ${columnNames(v3byvarRed)}; $v3byvarRedAvg; $v3byvarRedVar; ${columnNames(v1byvar)}; $v1byvarAvg; $v1byvarVar; ${columnNames(v2byvar)}; $v2byvarAvg; $v2byvarVar; ${columnNames(v3byvar)}; $v3byvarAvg; $v3byvarVar")
      }

      println((0 until summary.mean.size).map(x => columnNames(x)).mkString(";"))
      println("All dataset [Mean and Variance by column]:")
      println(summary.mean.toArray.mkString(";"))
      println(summary.variance.toArray.mkString(";"))
      summaryByKey.foreach{case (k,v) =>
        println("Cluster "+k)
        println(v.mean.toArray.mkString(";"))
        println(v.variance.toArray.mkString(";"))
      }


      // use countByKey

      logger.info("Total time to compute statistics was: " +(System.currentTimeMillis()-startingTime).toDouble/1000)
      
    }
  }
}