package it.polito.dbdmg.ontic

import it.polito.dbdmg.ontic.util.SilhouetteCommandLineParser
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import it.polito.dbdmg.ontic.io.ClusteredIOParser
import it.polito.dbdmg.ontic.postprocessing.SquaredSilhouette

object SquaredSilhouetteDriver {
  def main(args:Array[String]):Unit = {
    val clArgs:SilhouetteCommandLineParser = new SilhouetteCommandLineParser("Ontic SquaredSilhouette")
    if(clArgs.parse(args)){
      val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
      
      logger.setLevel(clArgs.args.onticLogLevel)
      Logger.getRootLogger.setLevel(clArgs.args.sparkLogLevel)
      
      //creating spark context
      var conf=new SparkConf().setMaster(clArgs.args.masterUrl)
                    .setAppName("Ontic Spark Squared Silhouette")
      
      logger.debug("Spark master: " +clArgs.args.masterUrl)
      
      implicit val sc=new SparkContext(conf)
      val startingTime = System.currentTimeMillis()
      
      //reading data
      val parser = Class.forName(clArgs.args.parserClassName).getConstructor().newInstance().asInstanceOf[ClusteredIOParser]
      parser.separator = clArgs.args.parserSeparator
      val startingPoints = sc.textFile(clArgs.args.inputPath).map { parser.parse }.filter(t=>t._1!=0)
      
      val points = if(clArgs.args.withNoisePoints){
        startingPoints.union(sc.textFile(clArgs.args.noiseInputPath).map { parser.parse }.filter(t=>t._1==0))
      }else{
        startingPoints
      }.cache()
      
      val sil = SquaredSilhouette.fromClusterIDAndCoordsRDD(points)
      println("SquaredSilhuette value is : " +sil.squaredSilhouette)
      println("----------------------------------------------------")
      sil.silhouettesForClusters.foreach( cluster => 
        println("Cluster "+cluster._1+" has squaredsilhouette "+cluster._2) )
      logger.info("Total time to compute squared silhouette is: " +(System.currentTimeMillis()-startingTime).toDouble/1000)
      
    }
  }
}