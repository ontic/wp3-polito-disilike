package it.polito.dbdmg.ontic

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.log4j.Logger
import org.apache.log4j.Level
import it.polito.dbdmg.ontic.io.ParseSettings
import it.polito.dbdmg.ontic.io.IOHandler
import it.polito.dbdmg.ontic.statistics.ZDistribution
import it.polito.dbdmg.ontic.statistics.MinimumsMaximums

object Stats {
  def main(args: Array[String]): Unit = {
    //creating spark context
    if(args.length != 2){
      println("Usage: <master-url> <hdfs-file>")
    }else{
      Logger.getRootLogger.setLevel(Level.WARN)
      val conf=new SparkConf().setMaster(args(0))
      .setAppName("Ontic Spark Statistics")
      val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
      logger.setLevel(Level.INFO)
      logger.info("Spark master: " +args(0))
      
      val sc=new SparkContext(conf)
      
      //set up parsing settings
      val parseSettings = new ParseSettings()
      .withSeparator(",")
      .withParser("it.polito.dbdmg.ontic.io.TextToPointConverter")
      
      val data = IOHandler.readTextFile(sc, parseSettings, args(1)).map(p=>p.coordinates.array)
      
      val z = ZDistribution(data)
      val minMax = MinimumsMaximums(data)
      
      
      println("Means : "+z.means.mkString(";"))
      println("Standard deviations : "+z.standardDeviations.mkString(";"))
      println()
      println("Minimums : "+minMax.mins.mkString(";"))
      println("Maximums : "+minMax.maxs.mkString(";"))
      
    }
  }
}