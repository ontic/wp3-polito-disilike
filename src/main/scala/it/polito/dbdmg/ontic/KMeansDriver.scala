package it.polito.dbdmg.ontic

import org.apache.spark.mllib.clustering.KMeans
import it.polito.dbdmg.ontic.util.KMeansCLParser
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import it.polito.dbdmg.ontic.io.ParseSettings
import it.polito.dbdmg.ontic.io.IOHandler
import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings
import it.polito.dbdmg.ontic.preprocessing.DBScanPreprocessor
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.linalg.Vectors
import org.alitouka.spark.dbscan.spatial.Point

object KMeansDriver {
  def main(args:Array[String]):Unit = {
    val clArgs:KMeansCLParser = new KMeansCLParser("Spark KMeans Driver")
    if(clArgs.parse(args)){
      
      //setting log levels
      val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
      
      logger.setLevel(clArgs.args.onticLogLevel)
      Logger.getRootLogger.setLevel(clArgs.args.sparkLogLevel)
      
      //creating spark context
      var conf=new SparkConf().setMaster(clArgs.args.masterUrl)
                    .setAppName("Spark KMeans Driver")
      
      logger.debug("Spark master: " +clArgs.args.masterUrl)
         
      
      val sc=new SparkContext(conf)
      
      //set up parsing settings
      val parseSettings = new ParseSettings()
                               .withSeparator(clArgs.args.parserSeparator)
                               .withParser(clArgs.args.parserClassName)
      
      logger.debug("Parse settings:\r\n\tseparator : " +parseSettings.textFileParser.separator+"\r\n\tclass : " +parseSettings.textFileParser.getClass.getCanonicalName )              
      
      val data = IOHandler.readTextFile(sc, parseSettings,clArgs.args.inputPath)
      val preprocessingSettings =  new DBSCANPreprocessingSettings()
                                          .withNormalizationTechnique(clArgs.args.normalizationTechnique.toString())
                                          
      val preprocessor = new DBScanPreprocessor(sc, data, preprocessingSettings)
      
      val normalizedData = preprocessor.normalize().map(p => Vectors.dense(p.coordinates.toArray))
      normalizedData.persist()
                    
      val kmeans = new KMeans()
      kmeans.setK(clArgs.args.K).setMaxIterations(1000).setRuns(100)
      // kmeans.setSeed(12345)
      val timestampKmeansStart: Long = System.currentTimeMillis / 1000
      val modelKmeans = kmeans.run(normalizedData)
      
      //una volta creato il modello, bisogna associare ad ogni record il cluster di appartenenza.
      //Dunque, per ogni record del dataset, effettuera' una predizione.
      val result = normalizedData.map {
        case datum =>
          //val cluster = modelKmeans.predict(datum)
          (new Point(datum.toArray)).withClusterId(modelKmeans.predict(datum)+1)
      }
      val realPoints = preprocessor.denormalize(result)
      IOHandler.saveClusteringResult(result, clArgs.args.outputPath + "/norm/")
      IOHandler.saveClusteringResult(realPoints, clArgs.args.outputPath + "/denorm/")
      val timestampKmeansStop: Long = System.currentTimeMillis / 1000
      logger.info("KMeans execution time :  " + (timestampKmeansStop - timestampKmeansStart)+" s.")

      val sse = modelKmeans.computeCost(normalizedData)
      Console.err.println("SSE:\n------------")
      println(clArgs.args.K+";"+sse.toString)


      val centroids = sc.parallelize(modelKmeans.clusterCenters).map{
        case datum =>
          //val cluster = modelKmeans.predict(datum)
          (new Point(datum.toArray)).withClusterId(modelKmeans.predict(datum)+1)
      }
      IOHandler.saveClusteringResult(centroids, clArgs.args.outputPath + "/modelKmeans")


    }
    
  }
}
