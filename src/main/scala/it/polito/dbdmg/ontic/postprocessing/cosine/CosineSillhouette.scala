package it.polito.dbdmg.ontic.postprocessing.cosine

import org.alitouka.spark.dbscan.spatial.Point
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.log4j.Logger

class CosineSilhouette(val silhouetteValues:RDD[(CosineSilhouetteNode, Double)]) {
  lazy val silhouette = {
    val data = silhouetteValues.aggregate((0L,0.0))((res,a)=>(res._1+1,res._2+a._2), (a,b)=>(a._1+b._1,b._2+a._2))
    data._2/data._1
  }
  lazy val silhouettesForClusters = {
    silhouetteValues.map(t => (t._1.clusterId, t._2))
                  .aggregateByKey((0L,0.0))( 
                      (res,sil) => (res._1+1L,res._2+sil) ,
                      (a,b)=>(a._1+b._1,a._2+b._2) )
                  .map( el => (el._1, el._2._2/el._2._1)).collectAsMap()
    
  }
}

object CosineSilhouette{
  
  private[CosineSilhouette] val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
  
  def fromDBSCANPoints(points:RDD[Point])(implicit sc:SparkContext):CosineSilhouette = {
    fromClusterIDAndCoordsRDD(points.map(p => (p.clusterId.toInt,p.coordinates.toArray[Double])))
  }
  
  def fromClusterIDAndCoordsRDD(points:RDD[(Int, Array[Double])])(implicit sc:SparkContext):CosineSilhouette = {
    apply(points.map( e => CosineSilhouetteNode(e._2,e._1,CosineSilhouetteNode.computeCsi(e._2))) )
  }
  def apply(points:RDD[CosineSilhouetteNode])(implicit sc:SparkContext):CosineSilhouette = {
    val clustersMap = CosineSilhouetteCluster.getClustersMap(points)
    val broadcastedClustersMap = sc.broadcast(clustersMap)
    val broadcastedClustersSet = sc.broadcast(clustersMap.keySet)
    new CosineSilhouette(points.map( node => 
      {
        var minOther = Double.MaxValue
        for(c <- broadcastedClustersSet.value){
          if(c!=node.clusterId){
            val sil = compute(node, broadcastedClustersMap.value.get(c).get)
            logger.trace("Squared Silhouette for node "+node.coordinates.mkString(",")+" and cluster "+c+" is "+sil)
            if(sil < minOther){
              minOther=sil
            }
          }
        }
        val clusterCurrentPoint = broadcastedClustersMap.value.get(node.clusterId).get
        
        val clusterSil = if(clusterCurrentPoint.getCount() == 1) 0 else compute(node, clusterCurrentPoint) * clusterCurrentPoint.getCount() / (clusterCurrentPoint.getCount()-1) //adjustment for excluding the node itself from the computation of the average dissimilarity
          
        logger.trace("Squared Silhouette for node "+node.coordinates.mkString(",")+" and its cluster "+node.clusterId+" is "+clusterSil)
        var silhouetteCoeff = 0.0
        if (clusterSil < minOther) {
          silhouetteCoeff = 1 - clusterSil / minOther
        }
        else {
          if (clusterSil > minOther) {
            silhouetteCoeff = minOther / clusterSil - 1
          }
        }
        (node, silhouetteCoeff)
      }).cache()
    )
  }
  
  
  def compute(node:CosineSilhouetteNode, cluster:CosineSilhouetteCluster) = {
    1-node.getCsi().zip(cluster.getOmega()).map((a)=>a._1*a._2).sum/cluster.getCount()
  }
  
}