package it.polito.dbdmg.ontic.postprocessing

import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext._
import org.apache.log4j.Logger

class Cluster(val clusterId:Int) extends Serializable {
  
  private[Cluster] var psi:Double = Double.NaN
  private[Cluster] var count:Long = 0L
  private[Cluster] var Y:Array[Double] = Array()
  
  def getY() = {
    assert(Y.length >0)
    Y
  }
  
  def getCount() = {
    assert(count > 0)
    count
  }
  
  def getPsi() = {
    assert(psi != Double.NaN)
    psi
  }
  
}

object Cluster{
  
  private[Cluster] val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
  
  private[Cluster] def apply(clusterId:Int, psi:Double, count:Long) = {
    val c = new Cluster(clusterId)
    c.psi = psi
    c.count=count
    c
  }
  
  private[Cluster] def generateClustersComputingPsiAndCount(points:RDD[(Int,Node)]) = {
    points.aggregateByKey((0.0,0L))((a,b)=>(a._1+b.getCsi(),a._2+1), (a,b)=>(a._1+b._1,a._2+b._2))
      .collectAsMap().map(tuple => { 
        //val c = new Cluster(tuple._1)
        //c.psi = tuple._2
        
        (tuple._1,Cluster(tuple._1, tuple._2._1, tuple._2._2))
      }
    )
  }
  
  private[Cluster] def computeYVectorForClusters(points:RDD[(Int,Node)]) = {
    val d = points.first()._2.coordinates.length
    points.aggregateByKey(Array.fill[Double](d)(0))(
        (res, e) => {
          res.zip(e.coordinates).map((t) => t._1+t._2)
        }, (a,b) => {
          a.zip(b).map((t) => t._1+t._2)
        }).collectAsMap()
  }
  
  private[Cluster] def mapPoints(points:RDD[Node])={points.map(a=>(a.clusterId,a))}
  
  /**
   * This method returns a map which associates each cluster id
   * to the relative [[it.polito.dbdmg.ontic.postprocessing.Cluster]]
   * object containing all the needed and precomputed informations
   * for the [[it.polito.dbdmg.ontic.postprocessing.SquaredSilhouette]].
   */
  def getClustersMap(points:RDD[Node]):scala.collection.Map[Int, Cluster] = {
    val nodes = mapPoints(points)
    val map = generateClustersComputingPsiAndCount(nodes)
    val Ymap = computeYVectorForClusters(nodes)
    
    for(t <- Ymap){
      map.get(t._1).get.Y = t._2
      logger.debug("Cluster "+t._1+": PSI = "+map.get(t._1).get.getPsi()+"; COUNT = "+map.get(t._1).get.getCount()+"; Y ="+t._2.mkString(","))
    }
    map
  }
  
}