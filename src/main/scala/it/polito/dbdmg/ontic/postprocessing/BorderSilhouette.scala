package it.polito.dbdmg.ontic.postprocessing

import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext._


object BorderSilhouette {
  def compute(clusterAndDatum : RDD[(Int, Array[Double])],
                       borderPoints : RDD[(Int, Array[Double])],
                       p:Double = 1.0): RDD[(Int, Array[Double], Double)] = {
    //i've to use an array because nested RDD is not permitted

    val clustersIds = borderPoints.map(t => t._1).distinct().collect()

    val borderPointsMap = borderPoints.sampleByKey(false, clustersIds.map((_, p)).toMap).groupByKey().collectAsMap()

    clusterAndDatum.map {
      case (cluster, datum) =>
        val sumOfDistance = borderPointsMap(cluster).map(d => euclideanDistance(datum, d)).sum[Double]
        val countOfNeighbor = borderPointsMap(cluster).size
        //println("Sum of distances "+sumOfDistance)
        //println("count neigh " +countOfNeighbor)
        val intraDistance = sumOfDistance / (countOfNeighbor - 1)
        val interDistance = clustersIds.filter { c => c != cluster }.
          map { k_ =>
            val sumOfInterDistance = borderPointsMap(k_).map { datum_ =>
              //println("distance between : "+datum+"   "+datum_)
              euclideanDistance(datum, datum_)
            }.sum[Double]
            val countOfInterPoints = borderPointsMap(k_).size
            sumOfInterDistance / countOfInterPoints
          }
        val minInterDistance = interDistance.min
        var silhouetteCoeff = 0.0
        if (intraDistance < minInterDistance) {
          silhouetteCoeff = 1 - (intraDistance / minInterDistance)
        }
        else {
          if (intraDistance > minInterDistance) {
            silhouetteCoeff = (minInterDistance / intraDistance) - 1
          }
        }
        (cluster, datum, silhouetteCoeff)

    }
  }

  def takeBorderFromCentroids(clusterAndDatum : RDD[(Int, Array[Double])],
                              centroids : Array[(Int, Array[Double])],
                              p:Int = 100): Array[(Int, Array[Double])] = {
    val k =centroids.length
    val centroidsMap = centroids.toMap
    val clusterAndDatumOnBorder = (1 to k).map { numberOfCluster =>
      val clusterPoints = clusterAndDatum.filter { case (cluster, datum) => cluster == numberOfCluster }
      val distanceAndClusterAndDatum = clusterPoints.map { case (cluster, datum) =>
        val d = euclideanDistance(datum, centroidsMap(numberOfCluster))
        (d, cluster, datum)
      }
      val res = distanceAndClusterAndDatum.takeOrdered(p)(Ordering[Double].reverse.on(x=>x._1)).map { case (d, cluster, datum) => (cluster, datum) }
      res
    }
    clusterAndDatumOnBorder.flatten.toArray
  }

  private def euclideanDistance(a: Array[Double], b: Array[Double]) =
    math.sqrt(a.zip(b).map(p => p._1 - p._2).map(d => d * d).sum)



}
