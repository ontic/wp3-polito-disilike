package it.polito.dbdmg.ontic.postprocessing

/**
 * Class representing a point, with some additional infos
 * useful for the squared silhouette computation.
 */
class Node (val coordinates:Array[Double], val clusterId:Int) extends Serializable {
  
  /**
   * This value contains the sum 
   * of the squares of the coordinates of the point,
   * but it is not initialized: it has to be initialized
   * with the computeCsi method of the 
   * [[it.polito.dbdmg.ontic.postprocessing.Node]] object.
   * 
   * This is basically the square of the norm of the vector.
   * 
   */
  private[Node] var csi:Double = Double.NaN
  
  def getCsi() = {
    assert ( csi != Double.NaN)
    csi
  }
  
  
}

object Node {
  
  def apply(coordinates:Array[Double], clusterId:Int, csi:Double) = {
    val n=new Node(coordinates, clusterId)
    n.csi = csi
    n
  }
  
  def computeCsi(coordinates:Array[Double]):Double = {
    var sum=0.0
    for(i <- coordinates){
      sum+=i*i
    }
    sum
  }
  
}

