package it.polito.dbdmg.ontic.io

import org.apache.log4j.Logger

class SilhouetteResultParser extends Serializable {
  var separator:String = ","
  def parse(line: String): (Int,Array[Double], Double) = {
    val els = line.split("\t")
    if(els.length == 3){
      try{
        val clusterId = els(1).toInt
        
        val coords = els(0).split(separator).map { x => x.toDouble }

        val silhouette = els(2).toDouble
        
        return (clusterId,coords, silhouette)
      }catch{
        case e:NumberFormatException => {
          Logger.getLogger("it.polito.dbdmg.ontic.Logger").error("NumberFormatException while parsing data: "+e.getMessage)
          
        }
      }
      
    }
    return null
    
  }
}