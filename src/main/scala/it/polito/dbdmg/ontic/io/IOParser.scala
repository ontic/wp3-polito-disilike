package it.polito.dbdmg.ontic.io

import org.alitouka.spark.dbscan.spatial.Point

trait IOParser extends Serializable {
  var separator:String = ","
  def parse(line:String):Point
}