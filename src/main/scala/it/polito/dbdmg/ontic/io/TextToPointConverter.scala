package it.polito.dbdmg.ontic.io

import org.alitouka.spark.dbscan.spatial.Point



 /** @author mark9
 *
 */
class TextToPointConverter extends IOParser with Serializable{
  
  override def parse(line:String):Point = {
    
    val els = line.split(separator)

    try{
      return new Point(els.map(x=>x.toDouble))
    }catch {
      case e :NumberFormatException => { 
        e.printStackTrace()
        return null
       }
    }
     
    null
    
  }
  
}