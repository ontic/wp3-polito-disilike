package it.polito.dbdmg.ontic.io

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat
import org.alitouka.spark.dbscan.DbscanModel
import org.apache.spark.rdd.RDD
import org.alitouka.spark.dbscan.spatial.Point

object IOHandler {

  def readTextFile(sc:SparkContext, settings:ParseSettings, path:String) = {
    sc.textFile(path)
      .map(settings.textFileParser.parse)
      .filter { x => x != null }
  }
  
  def saveClusteringResult (model: DbscanModel, outputPath: String) {

    model.allPoints.map ( pt => {

      pt.coordinates.mkString(";") + "\t" + pt.clusterId
    } ).saveAsTextFile(outputPath)
  }
  
  def saveClusteringResult (pts: RDD[Point], outputPath: String) {

    pts.map ( pt => {

      pt.coordinates.mkString(";") + "\t" + pt.clusterId
    } ).saveAsTextFile(outputPath)
  }

  def saveSilhouetteResult (pts: RDD[(Int, Array[Double], Double)], outputPath: String) {

    pts.map ( pt => {

      pt._2.mkString(";") + "\t" + pt._1 + "\t" + pt._3 // coords : cluster id : silhouette
    } ).saveAsTextFile(outputPath)
  }


  def saveClusteringResultWithTextMapping (model: DbscanModel, outputPath: String, textPoints:RDD[TextPoint]) {

    model.allPoints
      .map(pt => (pt.coordinates, pt.clusterId))
      .join(textPoints.map(pt=>(pt.coordinates,pt.text)).distinct().groupByKey())
      .map ( pt => {
          pt._2._2.mkString("/") + "\t" + pt._2._1
    } ).saveAsTextFile(outputPath)
  }
  
  
}