package it.polito.dbdmg.ontic.io

import org.alitouka.spark.dbscan.spatial.Point

class TextPoint(coords:Array[Double]) extends Point(coords) {
  
  var text:String = ""
  
  
  override def toString():String = {
    text
  }
}