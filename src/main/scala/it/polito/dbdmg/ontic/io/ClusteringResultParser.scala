package it.polito.dbdmg.ontic.io

import org.apache.log4j.Logger

class ClusteringResultParser extends ClusteredIOParser with Serializable {
  def parse(line: String): (Int,Array[Double]) = {
    val els = line.split("\t")
    if(els.length == 2){
      try{
        val clusterId = els(1).toInt
        
        val coords = els(0).split(separator).map { x => x.toDouble }
        
        return (clusterId,coords)
      }catch{
        case e:NumberFormatException => {
          Logger.getLogger("it.polito.dbdmg.ontic.Logger").error("NumberFormatException while parsing data: "+e.getMessage)
          
        }
      }
      
    }
    return null
    
  }
}