package it.polito.dbdmg.ontic.io




class ParseSettings {
  
  var textFileParser : IOParser = new TextToPointConverter
  
  def withSeparator(s:String) = {
    textFileParser.separator = s
    this
  }

  def withParser(parser : String) = {
    val s=textFileParser.separator
    textFileParser = Class.forName(parser).getConstructor().newInstance().asInstanceOf[IOParser]
    textFileParser.separator=s
    this
  }
  
}