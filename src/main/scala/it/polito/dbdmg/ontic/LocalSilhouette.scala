package it.polito.dbdmg.ontic

import org.apache.spark.rdd.RDD
import org.apache.spark.rdd.DoubleRDDFunctions
import it.polito.dbdmg.ontic.util.SilhouetteCommandLineParser
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import it.polito.dbdmg.ontic.io.ClusteredIOParser
import scala.collection.mutable.ListBuffer
import scala.Array

/**
 * @author mark9
 */
object LocalSilhouette {
  
  def main(args:Array[String]):Unit = {
    val clArgs:SilhouetteCommandLineParser = new SilhouetteCommandLineParser("LocalSilhouette")
    if(clArgs.parse(args)){
      val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
      
      logger.setLevel(clArgs.args.onticLogLevel)
      Logger.getRootLogger.setLevel(clArgs.args.sparkLogLevel)
      
      //creating spark context
      var conf=new SparkConf().setMaster(clArgs.args.masterUrl)
                    .setAppName("Ontic Spark DBSCAN")
      
      logger.debug("Spark master: " +clArgs.args.masterUrl)
      
      implicit val sc=new SparkContext(conf)
      val startingTime = System.currentTimeMillis()
      
      //reading data
      val parser = Class.forName(clArgs.args.parserClassName).getConstructor().newInstance().asInstanceOf[ClusteredIOParser]
      parser.separator = clArgs.args.parserSeparator
      val startingPoints = sc.textFile(clArgs.args.inputPath).map { parser.parse }.filter(t=>t._1!=0)
      
      val points = if(clArgs.args.withNoisePoints){
        startingPoints.union(sc.textFile(clArgs.args.noiseInputPath).map { parser.parse }.filter(t=>t._1==0))
      }else{
        startingPoints
      }
      val (silhouette, clustersSilhouette) = silhouetteStandard(points)
      println("Silhouette value is : " +silhouette)
      println("----------------------------------------------------")
      clustersSilhouette.foreach( cluster => 
        println("Cluster "+cluster._1+" has silhouette "+cluster._2) )
      logger.info("Total time to compute silhouette is: " +(System.currentTimeMillis()-startingTime).toDouble/1000)
      
    }
  }
  
  
  def euclideanDistance(a: Array[Double], b: Array[Double]) =
    math.sqrt(a.zip(b).map(p => p._1 - p._2).map(d => d * d).sum)
  

  
  def silhouetteStandard(clusterAndDatum : RDD[(Int, Array[Double])] )   = {
    //i've to use an array because nested RDD is not permitted
    
    val clustersIds = clusterAndDatum.map(t => t._1).distinct().collect()
    
    val clusterAndDatumMap = clusterAndDatum.groupByKey().collectAsMap()
    val clusterAndDatumAndSilhouette = clusterAndDatumMap.flatten {
      case (cluster, datums) => {
        val res:ListBuffer[(Int,Double)]= ListBuffer.empty
        datums.foreach { datum => 
          val sumOfDistance = clusterAndDatumMap(cluster).map(d => euclideanDistance(datum, d)).sum[Double]
          val countOfNeighbor = clusterAndDatumMap(cluster).size
          //println("Sum of distances "+sumOfDistance)
          //println("count neigh " +countOfNeighbor)
          val intraDistance = sumOfDistance / (countOfNeighbor - 1)
          val interDistance = (clustersIds).filter { c => c != cluster }.
            map { k_ =>
            val sumOfInterDistance = clusterAndDatumMap(k_).map { datum_ =>
              //println("distance between : "+datum+"   "+datum_)
              euclideanDistance(datum, datum_) }.sum[Double]
            val countOfInterPoints = clusterAndDatumMap(k_).size
            sumOfInterDistance / countOfInterPoints
          }
          val minInterDistance = interDistance.min
          var silhouetteCoeff = 0.0
          if (intraDistance < minInterDistance) {
            silhouetteCoeff = 1 - (intraDistance / minInterDistance)
          }
          else {
            if (intraDistance > minInterDistance) {
              silhouetteCoeff = (minInterDistance / intraDistance) - 1
            }
          }
          res.+=((cluster,silhouetteCoeff))
        }
        res
      }
    }.toArray
    //clusterAndDatumAndSilhouette.foreach(f=>println("sil "+f._1+"  "+f._2))
    val silhouettesForClusters = clusterAndDatumAndSilhouette.groupBy(a=>a._1).map( cluster => (cluster._1,cluster._2.map(a=>a._2).sum/cluster._2.size))
    val result=clusterAndDatumAndSilhouette.map(d=>d._2).sum/clusterAndDatumAndSilhouette.size
    (result , silhouettesForClusters)
  }
}