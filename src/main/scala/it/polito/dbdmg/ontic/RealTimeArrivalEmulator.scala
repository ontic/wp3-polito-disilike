package it.polito.dbdmg.ontic

import it.polito.dbdmg.ontic.io._
import it.polito.dbdmg.ontic.postprocessing.BorderSilhouette
import it.polito.dbdmg.ontic.preprocessing.{DBSCANPreprocessingSettings, DBScanPreprocessor}
import it.polito.dbdmg.ontic.util.RealTimeCommandLineParser
import org.apache.log4j.Logger
import org.apache.spark.SparkContext._
import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.stat.MultivariateOnlineSummarizer
import org.apache.spark.mllib.tree.DecisionTree
import org.apache.spark.mllib.tree.model.{Node, DecisionTreeModel}
import org.apache.spark.mllib.util.MLUtils._
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * @author mark9
 */
object RealTimeArrivalEmulator {



  def main(args:Array[String]):Unit = {
    val clArgs = new RealTimeCommandLineParser("RealTimeArrivalEmulator")
    if(clArgs.parse(args)){
      val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
      
      logger.setLevel(clArgs.args.onticLogLevel)
      Logger.getRootLogger.setLevel(clArgs.args.sparkLogLevel)
      
      ////creating spark context
      val conf=new SparkConf().setMaster(clArgs.args.masterUrl)
                    .setAppName("Ontic Spark DBSCAN")
      
      logger.debug("Spark master: " +clArgs.args.masterUrl)
      
      implicit val sc=new SparkContext(conf)
      val startingTime = System.currentTimeMillis()
      
      ////reading data
      val parseSettings = new ParseSettings()
        .withSeparator(clArgs.args.parserSeparator)
        .withParser(clArgs.args.parserClassName)
      val silParser = new SilhouetteResultParser
      val clustersParser = new ClusteringResultParser
      silParser.separator = ";"
      clustersParser.separator = ";"

      val trainingDataRaw = IOHandler.readTextFile(sc, parseSettings,clArgs.args.trainingInputPath)
      val preprocessingSettings =  new DBSCANPreprocessingSettings()
        .withNormalizationTechnique(clArgs.args.normalizationTechnique.toString())
      val normalizer = new DBScanPreprocessor(sc, trainingDataRaw, preprocessingSettings)
      //val normalizer = sc.objectFile[DBScanPreprocessor](normalizerPath).first()

      val silhouettes = sc.textFile(clArgs.args.silhouettesPath).map { silParser.parse }.filter(t=>t._1!=0)
      silhouettes.cache()
      val rtPointsBefore = IOHandler.readTextFile(sc, parseSettings,clArgs.args.inputPath)
      val rtPoints = normalizer.normalize(rtPointsBefore).map(x => Vectors.dense(x.coordinates.toArray))
      val borderPoints:RDD[(Int, Array[Double])] = if(clArgs.args.borderPath != null) {
        val borderPoints = sc.textFile(clArgs.args.borderPath).map {
          clustersParser.parse
        }.filter(t => t._1 != 0)
        val clustersWithNoBorder = silhouettes.map(_._1).distinct().subtract(borderPoints.map(_._1)).collect().toSet
        silhouettes.filter(x => clustersWithNoBorder.contains(x._1)).map{case(x,y,_) => (x,y)}.union(borderPoints)
      } else {
        val centroids = sc.textFile(clArgs.args.modelPath).map { clustersParser.parse }.collect()
        sc.parallelize(BorderSilhouette.takeBorderFromCentroids(
          silhouettes.map{case(cl, coord, _) => (cl, coord)}, centroids, 10000))
      }
      borderPoints.cache()
      val clusterId2Label = borderPoints.map(_._1).distinct().zipWithIndex().collectAsMap()
      val clusterIds = clusterId2Label.keys.toList
      val clusterLabel2Id = clusterId2Label.map{case (x, y) => (y, x)}
      val numClusters = clusterId2Label.size
      val trainingData = silhouettes.map {
        case(cluster, coords, _) =>
          LabeledPoint(clusterId2Label(cluster),Vectors.dense(coords))
      }

      ////Classification
      val timestampTreeStart: Long = System.currentTimeMillis
      val modelTree = DecisionTree.trainClassifier(trainingData, numClusters,  Map[Int,Int](), "gini", 4, 100 )
      val timestampTreeStop: Long = System.currentTimeMillis
      println("Execution time to train the tree :  " + (timestampTreeStop - timestampTreeStart))

      println(modelTree.toDebugString)
      println("Mapping tree labels <====> cluster ids:")
      println(clusterLabel2Id)
      println("Impurity at nodes:")
      printImpurity(modelTree.topNode)


      ////cross-validation
      val timestampCVStart: Long = System.currentTimeMillis
      crossValDecisionTree(trainingData, numClusters)
      val timestampCVStop: Long = System.currentTimeMillis
      println("Execution time for CROSS VALIDATION  : " + (timestampCVStop - timestampCVStart))

      var summarizer = silhouettes.aggregate(new MultivariateOnlineSummarizer)(
      {case (s, v) => s.add(Vectors.dense(v._3))},
      {case (s1, s2) => s1.merge(s2)}
      )
      logger.info("Loaded "+summarizer.count+ " silhouette values")
      val summarizerByCluster = silhouettes.keyBy(_._1).aggregateByKey(new MultivariateOnlineSummarizer)(
      {case (s, v) => s.add(Vectors.dense(v._3))},
      {case (s1, s2) => s1.merge(s2)}
      ).collectAsMap()


      def recursiveRTEvaluation (realTimeData : RDD[Vector]) : Double = {

        val ts0: Long = System.currentTimeMillis
        val rr = clArgs.args.refreshRate
        if(realTimeData.count() < rr){
          return 0
        }

        val b_i = realTimeData.take(rr)
        val otherRTData = realTimeData.zipWithIndex().filter{
          case(v, i) =>
            i>rr
        }.map{case (v,i) => v}
        val b_iRDD = sc.makeRDD(b_i)
        val ts1: Long = System.currentTimeMillis


        val predictedRealTimeData = b_iRDD.map{
          case(datum) =>
            val label = modelTree.predict(datum)
            (clusterLabel2Id(label.toLong), datum.toArray)
        }
        val ts2: Long = System.currentTimeMillis


        val p = clArgs.args.fraction
        val pointsAndSils = BorderSilhouette.compute(predictedRealTimeData, borderPoints, p)
        val ts4: Long = System.currentTimeMillis
        //val newData = pointsAndSils.union(oldData)
        val ts5: Long = System.currentTimeMillis
        val summarizerByCluster = pointsAndSils.keyBy(_._1).aggregateByKey(new MultivariateOnlineSummarizer)(
        {case (s, v) => s.add(Vectors.dense(v._3))},
        {case (s1, s2) => s1.merge(s2)}
        ).collectAsMap()
        val newsummarizer = summarizerByCluster.values.fold(new MultivariateOnlineSummarizer)(_.merge(_))
        summarizer = summarizer.merge(newsummarizer)
        val avgSil = summarizer.mean(0)
        val newAvgSil = newsummarizer.mean(0)

        val tsEnd: Long = System.currentTimeMillis
        println("Silhouette for "+summarizer.count+" from time 0     : "+avgSil+" : computed in (ms) [prepare|predict|silhouette|union|avg]     : "+(ts1-ts0)+" : "+(ts2-ts1)+" : "+(ts4-ts2)+" : "+(ts5-ts4)+" : "+(tsEnd-ts5))
        println("Silhouette for the last "+newsummarizer.count+" flows     : "+newAvgSil+" : computed in (ms) [prepare|predict|silhouette|union|avg]     : "+(ts1-ts0)+" : "+(ts2-ts1)+" : "+(ts4-ts2)+" : "+(ts5-ts4)+" : "+(tsEnd-ts5))
        val m = summarizerByCluster.mapValues(_.mean(0))
        println("Silhouette for clusters at "+summarizer.count+" : "+clusterIds.map(x => m.getOrElse(x, "-")).mkString(":"))
        val c = summarizerByCluster.mapValues(_.count)
        println("Number of predictions for clusters at "+summarizer.count+" : "+clusterIds.map(x => c.getOrElse(x, "0")).mkString(":"))

        recursiveRTEvaluation( otherRTData)

      }




      println("Clusters: "+clusterIds.mkString(":"))
      val avgSil = summarizer.mean(0)
      println("Silhouette at time 0 : "+avgSil)
      val m = summarizerByCluster.mapValues(_.mean(0))
      val c = summarizerByCluster.mapValues(_.count)
      println("Silhouette for clusters at time 0 : "+clusterIds.map(x => m.getOrElse(x, "-")).mkString(":"))
      println("Number of predictions for clusters at time 0 : "+clusterIds.map(x => c.getOrElse(x, "0")).mkString(":"))
      recursiveRTEvaluation( rtPoints )
      
    }
  }

  def printImpurity(anode: Node, prefix: String = "") : Any = {

    println(prefix+anode.impurity.toString)

    if (anode.leftNode.isDefined) {
      printImpurity(anode.leftNode.get, prefix + " ")
    }
    if (anode.rightNode.isDefined) {
      printImpurity(anode.rightNode.get, prefix + " ")
    }


  }

  def crossValDecisionTree(data : RDD[LabeledPoint], numClusters:Int, nFolds : Int = 3, seed : Int = 77) = {

    val folds = kFold(data, nFolds, seed)

    val models = folds.map { case (train, test) => (DecisionTree.trainClassifier(train, numClusters,  Map[Int,Int](), "gini", 4, 100), test) }

    val accuracyAndPrecisionAndRecall = models.map{
      case (model, testData) =>
        val metrics = getMetrics(model, testData)
        val accuracy = metrics.precision

        val precisionAndRecall = (0 until numClusters).map {
          cat =>
            val precision = metrics.precision(cat)
            val recall = metrics.recall(cat)
            (precision,recall)
        }.toArray
        println("Accuracy: " + accuracy)
        precisionAndRecall.map(x=>x).foreach(println)
        (accuracy , precisionAndRecall)
    }

    accuracyAndPrecisionAndRecall
  }

  private def getMetrics(modelTree: DecisionTreeModel, data: RDD[LabeledPoint]):
  MulticlassMetrics = {
    val predictionsAndLabels = data.map(example =>
      (modelTree.predict(example.features), example.label)
    )
    new MulticlassMetrics(predictionsAndLabels)
  }

}
