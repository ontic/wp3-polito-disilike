package it.polito.dbdmg.ontic

import org.scalatest.FunSuite
import it.polito.dbdmg.ontic.util.CommandLineParser
import org.alitouka.spark.dbscan.spatial.CosineAngleDistanceSuite
import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings.EpsilonPolicies
import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings.MinPointsPolicies
import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings.NormalizationTechnique
import org.apache.log4j.Level

class CommandLineParserTester extends FunSuite {
  
  test("complete test"){
    val argsParser = new CommandLineParser("ParseTesting")
    
    
    val args =  Array[String](
        "--ds-master", "spark://dbdmg:7070/",
        "--ds-input","hdfs://user/gaido/input",
        "--ds-output","hdfs://user/gaido/output",
        "--distanceMeasureSuite","org.alitouka.spark.dbscan.spatial.CosineAngleDistanceSuite",
        "--epsStep","0.01",
        "--epsPolicy","BeforeMaxDensityDecrease",
        "--minPointsPolicy","MyGreedyFunc",
        "--clusteringLevels","2",
        "--normalization","Zscore",
        "--parserClass", "it.polito.dbdmg.ontic.io.RapidminerTextConverter",
        "--parserSeparator", ";",
        "--sparkLoggerLevel","INFO",
        "--onticLoggerLevel","DEBUG"
    )
    argsParser.parse(args)
    
    assert(argsParser.args.masterUrl=="spark://dbdmg:7070/")
    assert(argsParser.args.inputPath=="hdfs://user/gaido/input")
    assert(argsParser.args.outputPath=="hdfs://user/gaido/output")
    assert(argsParser.args.distanceMeasureSuite.isInstanceOf[org.alitouka.spark.dbscan.spatial.CosineAngleDistanceSuite])
    assert(argsParser.args.epsilonStep==0.01)
    assert(argsParser.args.epsilonPolicy==EpsilonPolicies.BeforeMaxDensityDecrease)
    assert(argsParser.args.minPtsPolicy==MinPointsPolicies.MyGreedyFunc)
    assert(argsParser.args.numberOfClusteringLevels==2)
    assert(argsParser.args.normalizationTechnique==NormalizationTechnique.Zscore)
    assert(argsParser.args.parserClassName=="it.polito.dbdmg.ontic.io.RapidminerTextConverter")
    assert(argsParser.args.parserSeparator==";")
    assert(argsParser.args.sparkLogLevel==Level.INFO)
    assert(argsParser.args.onticLogLevel==Level.DEBUG)
    
    
  }
  
  test("some missing values"){
    val argsParser = new CommandLineParser("ParseTesting")
    val args =  Array[String](
        "--ds-master", "spark://dbdmg:7070/",
        "--ds-input","hdfs://user/gaido/input",
        "--ds-output","hdfs://user/gaido/output",
        "--distanceMeasureSuite","org.alitouka.spark.dbscan.spatial.CosineAngleDistanceSuite",
        "--epsStep","0.01",
        "--epsPolicy","BeforeMaxDensityDecrease")
    
    argsParser.parse(args)
    
    assert(argsParser.args.masterUrl=="spark://dbdmg:7070/")
    assert(argsParser.args.inputPath=="hdfs://user/gaido/input")
    assert(argsParser.args.outputPath=="hdfs://user/gaido/output")
    assert(argsParser.args.distanceMeasureSuite.isInstanceOf[org.alitouka.spark.dbscan.spatial.CosineAngleDistanceSuite])
    assert(argsParser.args.epsilonStep==0.01)
    assert(argsParser.args.epsilonPolicy==EpsilonPolicies.BeforeMaxDensityDecrease)
    assert(argsParser.args.minPtsPolicy==MinPointsPolicies.MyGreedyFunc)
    assert(argsParser.args.numberOfClusteringLevels==3)
    assert(argsParser.args.normalizationTechnique==NormalizationTechnique.None)
    assert(argsParser.args.parserClassName=="it.polito.dbdmg.ontic.io.TextToPointConverter")
    assert(argsParser.args.parserSeparator==",")
    assert(argsParser.args.sparkLogLevel==Level.WARN)
    assert(argsParser.args.onticLogLevel==Level.INFO)
    
    
  }
  
}