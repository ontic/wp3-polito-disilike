package it.polito.dbdmg.ontic

import org.scalatest.FunSuite
import org.apache.log4j.{Logger,Level}
import org.apache.spark.{SparkConf,SparkContext}
import it.polito.dbdmg.ontic.io.{ParseSettings,IOHandler}
import org.alitouka.spark.dbscan.{DbscanSettings,Dbscan}
import it.polito.dbdmg.ontic.preprocessing.{DBScanPreprocessor,DBSCANPreprocessingSettings}
import org.alitouka.spark.dbscan.spatial.Point
import org.alitouka.spark.dbscan.util.io.IOHelper
import it.polito.dbdmg.ontic.io.RapidminerTextConverter
import it.polito.dbdmg.ontic.io.TextPoint
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext._
import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings.NormalizationTechnique
import org.alitouka.spark.dbscan.spatial.rdd.PointsPartitionedByRegionRDD
import org.alitouka.spark.dbscan.spatial.rdd.PartitioningSettings
import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.apache.spark.mllib.linalg.Vectors
import it.polito.dbdmg.ontic.statistics.MinimumsMaximums
import org.alitouka.spark.dbscan.DbscanModel



class CrozzaSmall extends FunSuite {
 
  val conf=new SparkConf().setMaster("local[*]").setAppName("Test")//.set("DebugOutputPath", "/Users/mark9/debug/debug")
  val sc=new SparkContext(conf)
  /*
  test("testing the parser"){
    
    val parser = new RapidminerTextConverter();
    parser.separator=";"
    val s = "0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.3769938609692219;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.15604977376899642;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.2529471320568777;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.37367890446588514;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.2279134021457098;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.3769938609692219;0.0;0.3240627942923398;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.38050392432474917;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.232420465430967;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.3633425762709159;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;\"microsatir mentr ital annasp melm question fondamental cos dir crozz pap \";\"[35999.8 - 39599.6]\""
    val p=parser.parse(s)
    assert(p.coordinates.length==707)
    assert(p.coordinates(0)==0.0)
    assert(p.coordinates(28)!=0.0)
    assert(p.coordinates.forall { x => x==0.0 } == false)
    
  }
  
  /*test("testing converting from coords to texts"){
    Logger.getRootLogger.setLevel(Level.WARN)
    val parseSettings = new ParseSettings()
                             .withSeparator(";")
                             .withParser("it.polito.dbdmg.ontic.io.RapidminerTextConverter")
    var data = sc.parallelize(IOHandler.readTextFile(sc, parseSettings,"/Users/mark9/crozza_full.csv")
                    .filter { x => ! x.coordinates.forall { x => x==0.0 } }.take(10))
                    
     val textData = data.cache()
     
     textData.foreach { x => {Console.println(x.asInstanceOf[TextPoint].text)} }
     
     data.map(pt => (pt.coordinates, pt.clusterId))
      .join(textData.map(pt=>(pt.coordinates,pt.asInstanceOf[TextPoint].text)).distinct())
      .map ( pt => {
          pt._2._2 + "\t" + pt._2._1
    } ).foreach(x => {Console.println(x)})
    
  }*/
  
  
  
  
  test("Executing DDBSCAN with Cosine Distance on the small Crozza dataset"){
    val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
    
    logger.setLevel(Level.INFO)
    Logger.getRootLogger.setLevel(Level.WARN)
    
    
    
    //reading data
    val parseSettings = new ParseSettings()
                             .withSeparator(";")
                             .withParser("it.polito.dbdmg.ontic.io.RapidminerTextConverter")
                             //.withSeparator("/")
    
    //var data = IOHandler.readTextFile(sc, parseSettings,"/Users/mark9/Desktop/slide_dataset.txt")
    //var data = IOHandler.readTextFile(sc, parseSettings,"/Users/mark9/example_paper.txt")
    var data = IOHandler.readTextFile(sc, parseSettings,"/Users/mark9/crozza_percent6.csv")
                    .filter { x => x.coordinates.exists { x => x!=0.0 } }
    
    
    val textData = data.cache()
    
    
    val startingTime = System.currentTimeMillis()
    //preprocessing
    val dbscanPreprocessingSettings =  new DBSCANPreprocessingSettings()
                                        .withEpsilonPolicy("BeforeFirstDensityDecrease")
                                        .withEpsStep(0.001)
                                        .withDistanceMeasureSuite("org.alitouka.spark.dbscan.spatial.CosineAngleDistanceSuite")
                                        //.withNumberOfPointsPerPartition(5000)
                                        //.withEpsilonPolicy("StdDevOverMean");
                                        //.withMinPointsPolicy("BeforeGreatestPercentageIncrement")
//    val preprocessor = new DBScanPreprocessor(sc, data, dbscanPreprocessingSettings)
//    val (minPts, eps) = preprocessor.preprocess()//single eps-minpts
//    //val params = preprocessor.preprocessMultiple()//multiple eps-minpts
//    val endPreprocessTime = System.currentTimeMillis()
//    logger.info("Total time to preprocess data: "+(endPreprocessTime-startingTime)/1000+" s")
//    var i:Int=0
//    //clustering
//    for ( (minPts,eps) <- params ){
//      val clusteringSettings = new DbscanSettings ().withEpsilon(eps).withNumberOfPoints(minPts+1)
//      val model = Dbscan.train (data, clusteringSettings)
//      IOHelper.saveClusteringResult(model, "/Users/mark9/output_folder"+i)
//    }
//    logger.info("Total time to process: "+(System.currentTimeMillis()-endPreprocessTime)/1000+" s")
//    
    
    for(i <- 1 to 3){
      //preprocess
      val startingTime = System.currentTimeMillis()
      val preprocessor = new DBScanPreprocessor(sc, data, dbscanPreprocessingSettings)
      val (minPts, eps) = preprocessor.preprocess()//single eps-minpts
      //val (minPts, eps) = (229, 0.7)
      val endPreprocessTime = System.currentTimeMillis()
      logger.info("Total time to preprocess data: "+(endPreprocessTime-startingTime)/1000+" s")
      
      //clustering
      val clusteringSettings = new DbscanSettings ()
        .withEpsilon(eps).withNumberOfPoints(minPts+1)
        .withDistanceMeasureSuite(new org.alitouka.spark.dbscan.spatial.CosineAngleDistanceSuite)

      logger.debug("I am using "+minPts+" as minPoints and "+eps+" as eps")
      val model = Dbscan.train (data, clusteringSettings)
      IOHandler.saveClusteringResultWithTextMapping(model, "/Users/mark9/crozza_tweets/small_first/"+i, textData.asInstanceOf[RDD[TextPoint]])
      logger.info("Total time to process: "+(System.currentTimeMillis()-endPreprocessTime)/1000+" s")
      
      //update data
      data=model.noisePoints.map{ p => new Point(p.coordinates) }
      logger.info("Count of noise points is "+data.count())
      
    }
  }
    */
  test("Executing with net points"){
    val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
    
    logger.setLevel(Level.INFO)
    Logger.getRootLogger.setLevel(Level.WARN)
    
    
    val outputFolder = "/Users/mark9/ontic_result/"
    //reading data
    val parseSettings = new ParseSettings()
                             //.withSeparator("\t")
                             //.withSeparator("/")
    
    //var data = IOHandler.readTextFile(sc, parseSettings,"/Users/mark9/Desktop/slide_dataset.txt")
    var data = IOHandler.readTextFile(sc, parseSettings,"/Users/mark9/ontic_data/*.txt")
                
    //var data = IOHandler.readTextFile(sc, parseSettings,"hdfs://localhost:8020/user/mark9/polito/examples/test_points.txt")
        
    data = data
          //.filter(p => p.coordinates(16)==0)
          .map(
      p =>  {
        
        val coords = p.coordinates
        new Point(Array(coords(0),coords(3),coords(5),coords(7),coords(14),coords(15)/*,coords(16)*/))
        
      }
      
    )
    
    val startingTime = System.currentTimeMillis()
    //preprocessing
    val dbscanPreprocessingSettings =  new DBSCANPreprocessingSettings()
                                        .withEpsilonPolicy("BeforeFirstDensityDecrease")
                                        .withEpsStep(0.00001)
                                        .withNormalizationTechnique("MinMax")
    
                                        
    /*val pcaMat = new RowMatrix(data.map(p=>Vectors.dense(p.coordinates.toArray[Double])))
    
    val pc=pcaMat.computePrincipalComponents(3)
    data = pcaMat.multiply(pc).rows.map { x => new Point(x.toArray) }*/
    var preprocessor = new DBScanPreprocessor(sc, data, dbscanPreprocessingSettings)
    val normalized = preprocessor.normalize()
    println("Normalized "+normalized.count()+" points ")
    
   val partitioned=preprocessor.partitioning()
                                
    //partitioned.mapPartitionsWithIndex((i,x)=>x.map(a=>(i,a))).aggregateByKey(0L)((x,y)=>x+1,(x,y)=>x+y)
    //.foreach(p => println("partition "+p._1+" has "+p._2+" points"))
    //partitioned.boxes.foreach { x => println("partition "+x.partitionId+" has these bounds "+x.bounds.mkString(";")) }
    
    val zeroPartition = partitioned.mapPartitionsWithIndex((i,p)=>p.map(t=>(i,t._2)), true)
            .map(p => if(p._1 == 0) p._2.withClusterId(10) else p._2.withClusterId(DbscanModel.NoisePoint) )
    
    
    
    //IOHandler.saveClusteringResult(zeroPartition, outputFolder+"0")
    
    data = zeroPartition.filter { x => x.clusterId == DbscanModel.NoisePoint }.map( p => new Point(p.coordinates))
      
    println("data is "+data.count())
    
    
    
    for(i <- 1 to 3){
      //preprocess
      val startingTime = System.currentTimeMillis()
      preprocessor = new DBScanPreprocessor(sc, data, dbscanPreprocessingSettings)

     
                                  
      val (minPts, eps) = preprocessor.preprocess()//single eps-minpts
      //val (minPts, eps) = (10306, 0.0025678114834092394)
      val endPreprocessTime = System.currentTimeMillis()
      logger.info("Total time to preprocess data: "+(endPreprocessTime-startingTime)/1000+" s")
      
      //clustering
      val clusteringSettings = new DbscanSettings ()
        .withEpsilon(eps).withNumberOfPoints(minPts+1)

      logger.debug("I am using "+minPts+" as minPoints and "+eps+" as eps")
      val model = Dbscan.train (data, clusteringSettings)
      IOHandler.saveClusteringResult(model, outputFolder+i)
      logger.info("Total time to process: "+(System.currentTimeMillis()-endPreprocessTime)/1000+" s")
      
      //update data
      data=model.noisePoints.map{ p => new Point(p.coordinates) }
      logger.info("Count of noise points is "+data.count())
      
    }
  }
  
}