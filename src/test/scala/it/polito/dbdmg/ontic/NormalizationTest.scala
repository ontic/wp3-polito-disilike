package it.polito.dbdmg.ontic

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.scalatest.FunSuite
import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings
import org.alitouka.spark.dbscan.spatial.Point
import it.polito.dbdmg.ontic.preprocessing.DBScanPreprocessor

class NormalizationTest extends FunSuite  {
    val conf=new SparkConf().setMaster("local[*]").setAppName("Test")//.set("DebugOutputPath", "/Users/mark9/debug/debug")
    val sc=new SparkContext(conf)
    
    test("Testing z-score method"){
      val dataset = sc.parallelize(Array(
          new Point(1.0, 4.0, 3.0), new Point(6.5, 3.4, 9.0),
          new Point(7.1,4.3,8.0), new Point(12.0,11.0,4.0),
          new Point(5.0, 6.0, 7.7), new Point(4.4,5.5,6.6)))
      val settings = new DBSCANPreprocessingSettings().withNormalizationTechnique("Zscore")
      
      val preprocessor = new DBScanPreprocessor(sc, dataset, settings)
      
      val normalized = preprocessor.normalize()
      
      normalized.foreach { x => println(x.coordinates.mkString(";")) }
      
      
    }
    
    test("Testing min-max method"){
      val dataset = sc.parallelize(Array(
          new Point(1.0, 4.0, 3.0), new Point(6.5, 3.4, 9.0),
          new Point(7.1,4.3,8.0), new Point(12.0,11.0,4.0),
          new Point(5.0, 6.0, 7.7), new Point(4.4,5.5,6.6)))
      val settings = new DBSCANPreprocessingSettings().withNormalizationTechnique("MinMax")
      
      val preprocessor = new DBScanPreprocessor(sc, dataset, settings)
      
      val normalized = preprocessor.normalize()
      
      normalized.foreach { x => println(x.coordinates.mkString(";")) }
      
      
    }
    
    test("Testing preprocessing interaction method"){
      val dataset = sc.parallelize(Array(
          new Point(1.0, 4.0, 3.0), new Point(6.5, 3.4, 9.0),
          new Point(7.1,4.3,8.0), new Point(12.0,11.0,4.0),
          new Point(5.0, 6.0, 7.7), new Point(4.4,5.5,6.6)))
      val settings = new DBSCANPreprocessingSettings().withNormalizationTechnique("MinMax")
                      .withEpsStep(0.01)
      val preprocessor = new DBScanPreprocessor(sc, dataset, settings)
      
      val normalized = preprocessor.normalize()
      val (minPts, eps) = preprocessor.preprocess()
      println("min pts : "+minPts+" and eps "+eps)
      
      normalized.foreach { x => println(x.coordinates.mkString(";")) }
      
      
    }
}