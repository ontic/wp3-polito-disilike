package it.polito.dbdmg.ontic

import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.scalatest.FunSuite
import it.polito.dbdmg.ontic.io.ParseSettings
import it.polito.dbdmg.ontic.io.IOHandler
import org.alitouka.spark.dbscan.spatial.Point
import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings
import it.polito.dbdmg.ontic.preprocessing.DBScanPreprocessor

class Histograms extends FunSuite {
 
  val conf=new SparkConf().setMaster("local[*]").setAppName("Test")//.set("DebugOutputPath", "/Users/mark9/debug/debug")
  val sc=new SparkContext(conf)
  
  test("Executing with net points"){
    val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
    
    logger.setLevel(Level.DEBUG)
    Logger.getRootLogger.setLevel(Level.WARN)
    
    
    
    //reading data
    val parseSettings = new ParseSettings()
                             //.withSeparator("\t")
                             //.withSeparator("/")
    
    var data = IOHandler.readTextFile(sc, parseSettings,"/Users/mark9/ontic_data/*.txt")
                
        
    data = data
          .map(
      p =>  {
        
        val coords = p.coordinates
        new Point(Array(coords(0),coords(3),coords(5),coords(7),coords(14),coords(15)/*,coords(16)*/))
        
      }
    )
    val dbscanPreprocessingSettings =  new DBSCANPreprocessingSettings()
                                        .withNormalizationTechnique("MinMax")
    
    
    
                                        
    val preprocessor = new DBScanPreprocessor(sc, data, dbscanPreprocessingSettings)
    val normalized = preprocessor.normalize()
    
    for(i <- 0 to 5){
       val histogram = normalized.map ( p => ((p.coordinates(i)*50).toInt, 1 ))
                .aggregateByKey(0)(_+_, _+_).sortByKey()
       println("----------- dimension "+i+" ------------")
       histogram.collect().foreach( t => println((t._1*2).formatted("%3d")+" - "+((t._1+1)*2).formatted("%3d")+" %\t"+t._2) )
    }
    
    
  }
}