package it.polito.dbdmg.ontic

import org.scalatest.FunSuite
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import it.polito.dbdmg.ontic.postprocessing.Node
import it.polito.dbdmg.ontic.postprocessing.Cluster
import org.apache.log4j.Logger
import org.apache.log4j.Level
import it.polito.dbdmg.ontic.postprocessing.SquaredSilhouette
import it.polito.dbdmg.ontic.postprocessing.cosine.CosineSilhouette

class Silhouette extends FunSuite {
   val conf=new SparkConf().setMaster("local[*]").setAppName("Test")//.set("DebugOutputPath", "/Users/mark9/debug/debug")
   implicit val sc=new SparkContext(conf)
   
   
   test("testing csi computation"){
     
     val coords = Array(23.12, 3.5, 7.2, -0.4, 0.0)
     val node = Node(coords,1,Node.computeCsi(coords))
     assert(node.getCsi() == 598.7844)
     
   }
   
   val dataset:Array[(Int,Array[Double])] = Array(
     (1, Array(1.5,2.1,1.6)),
     (1, Array(0.4,3.8,1.5)),
     (1, Array(1.7,1.9,2.0)),
     (2, Array(-15.2,3.0,-12)),
     (2, Array(-18,3.8,-10)),
     (2, Array(-12,1.9,-13)),
     (3, Array(13,-1,14)),
     (3, Array(13.2,0.2,14.5)),
     (3, Array(13.9,-1.5,14.3))
    )
    
   test("testing psi and Y computation"){
     val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
    
     logger.setLevel(Level.DEBUG)
     Logger.getRootLogger.setLevel(Level.WARN)
     val points = sc.parallelize(dataset)
     val map = Cluster.getClustersMap(points.map(a =>Node(a._2,a._1,Node.computeCsi(a._2))))
     
     assert(map(1).getPsi() == 36.57)
     assert(map(2).getPsi() == 1139.09)
     assert(map(3).getPsi() == 1150.48)
     
     //assert(map(1).getY().sameElements(Array(3.6, 7.8, 5.1))) //- numeric error
     assert(map(2).getY().sameElements(Array(-45.2, 8.7, -35.0)))
     assert(map(3).getY().sameElements(Array(40.1, -2.3, 42.8)))
     
   }
   
   test("testing squared silhouette computation") {
     val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")

     logger.setLevel(Level.DEBUG)
     Logger.getRootLogger.setLevel(Level.WARN)
     val points = sc.parallelize(dataset)
     
     val sil = SquaredSilhouette.fromClusterIDAndCoordsRDD(points)
     //sil.silhouetteValues.foreach( s => Logger.getLogger("it.polito.dbdmg.ontic.Logger").debug("SquaredSilhouette coeff for point "+s._1.coordinates.mkString(",")+" is "+s._2))
     assert( sil.squaredSilhouette == 0.9760608836328311 )
     
   }
   
   test("testing cosine silhouette computation") {
     val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
     val dataset:Array[(Int,Array[Double])] = Array((1,Array(10, 0.75)), (0,Array(1, 1)), (1,Array(2, 0)), 
      (2,Array(3, 2)), (2,Array(0.5, 0.24)), (1,Array(1, 0.1)),
      (2,Array(7, 4)), (2,Array(5, 2)), (1,Array(100,-2)))
     logger.setLevel(Level.TRACE)
     Logger.getRootLogger.setLevel(Level.WARN)
     val points = sc.parallelize(dataset)
     
     val sil = CosineSilhouette.fromClusterIDAndCoordsRDD(points)
     sil.silhouetteValues.foreach( s => Logger.getLogger("it.polito.dbdmg.ontic.Logger").debug("CosineSilhouette coeff for point "+s._1.coordinates.mkString(",")+" is "+s._2))
     sil.silhouettesForClusters.foreach(x => println("cluster "+x._1+" has silhouette "+x._2))
     
     
   }
   
}